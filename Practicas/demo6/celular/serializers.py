from rest_framework import serializers

from .models import Celular 


class CelularSerialize(serializers.ModelSerializer):
	class Meta:
		model = Celular
		fields = [
			"modelo",
			"marca",
			"año",
			"so",
			"camaras",
			"compañias",
		]