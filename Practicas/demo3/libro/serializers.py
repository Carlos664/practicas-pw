from rest_framework import serializers

from .models import Libro 


class LibroSerialize(serializers.ModelSerializer):
	class Meta:
		model = Libro
		fields = [
			"titulo",
			"autor",
			"año",
			"genero",
			"numpaginas",
		]