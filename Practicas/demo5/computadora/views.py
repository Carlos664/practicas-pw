from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q


# Create your views here.

from .models import Computadora
from .forms import ComputadoraForm

##Modulos necesarios para APIS 
from .serializers import ComputadoraSerialize
from rest_framework import generics

#Vistas de las APIS
class ComputadoraAPICreate(generics.CreateAPIView):
	serializer_class = ComputadoraSerialize

	def perform_create(self , serializer):
		serializer.save()


class ComputadoraAPIList(generics.ListAPIView):
	serializer_class = ComputadoraSerialize

	def get_queryset(self , *args , **kwargs):
		return Computadora.objects.all()


# CREATE
def create(request):
	form = ComputadoraForm(request.POST or None)
	if request.user.is_authenticated:
		message = "User Is Logged"
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect("list")
	else:
		message = "User Must Be Logged"

	context = {
		"form": form,
		"message": message
	}
	return render(request, "computadora/create.html", context)

# RETRIEVE
def list(request):
	queryset = Computadora.objects.all()
	context = {
		"computadoras": queryset
	}
	return render(request, "computadora/list.html", context)

def detail(request, id):
 	queryset = Computadora.objects.get(id=id)	
 	context = {
 		"object": queryset
 	}
 	return render(request, "computadora/detail.html", context)


# UPDATE
def update(request, id):
	computadora = Computadora.objects.get(id=id)
	if  request.method == "GET":
		form = ComputadoraForm(instance=computadora )
	else:
		form = ComputadoraForm(request.POST, instance=computadora )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form": form
	}
	return render(request, "computadora/update.html", context)

# DELETE
def delete(request, id):
	computadora = Computadora.objects.get(id=id)
	if request.method == "POST":
		computadora.delete()
		return redirect("list")
	context = {
		"object": computadora
	}
	return render(request, "computadora/delete.html", context)

#vistas genericas
class List(generic.ListView):
	template_name = "computadora/list2.html"
	queryset = Computadora.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Computadora.objects.all()
		query = self.request.GET.get('q' , None)
		if(query):
			qs = qs.filter( Q( modelo__icontains = query ) )
		return qs

class Detail(generic.DetailView):
	template_name = "computadora/detail2.html"
	model = Computadora

class Create(generic.CreateView):
	template_name = "computadora/create2.html"
	model = Computadora
	fields = [
			"modelo",
			"marca",
			"procesador",
			"ram",
			"disco",
			"estado",
		]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "computadora/update2.html"
	model = Computadora
	fields = [
			"modelo",
			"marca",
			"procesador",
			"ram",
			"disco",
			"estado",
		]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "computadora/delete2.html"
	model = Computadora
	success_url = reverse_lazy("list")

