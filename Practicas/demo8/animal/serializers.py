from rest_framework import serializers

from .models import Animal 

class AnimalSerialize(serializers.ModelSerializer):
	class Meta:
		model = Animal
		fields = [
			"nombre",
			"tipo",
			"ecosistemas",
			"alimento",
			"extinto",
		]