from rest_framework import serializers

from .models import Pelicula 


class PeliculaSerialize(serializers.ModelSerializer):
	class Meta:
		model = Pelicula
		fields = [
			"titulo",
			"director",
			"año",
			"genero",
			"duracion",
			"sinopsis",
		]