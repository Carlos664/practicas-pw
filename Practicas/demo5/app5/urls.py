from django.urls import path
from app5 import views
from django.contrib.auth.views import logout_then_login

urlpatterns = [
    path('', views.index , name = 'index' ),
    path('about/', views.about , name = 'about' ),
    path('list/', views.lista , name = 'list' ),
    path('template1/', views.template1 , name = 't1' ),
    path('template2/', views.template2 , name = 't2' ),
    path('template3/', views.template3 , name = 't3' ),
    path('template4/', views.template4 , name = 't4' ),
    path('template5/', views.template5 , name = 't5' ),
    path('template6/', views.template6 , name = 't6' ),
    path('template7/', views.template7 , name = 't7' ),
    path('template8/', views.template8 , name = 't8' ),
    path('template9/', views.template9 , name = 't9' ),
    path('template10/', views.template10 , name = 't10' ),
    path('cerrar/' , logout_then_login ,name = 'logout'),
    path('signup/' , views.SignUp.as_view() , name = 'signup'),
]