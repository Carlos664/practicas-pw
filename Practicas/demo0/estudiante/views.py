from django.shortcuts import render, redirect

# Create your views here.
from .models import Estudiante
from .forms import EstudianteForm

#Importar vistas genericas 
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

##Modulos necesarios para APIS 
from .serializers import EstudianteSerialize
from rest_framework import generics

#Vistas de las APIS
class EstudianteAPICreate(generics.CreateAPIView):
	serializer_class = EstudianteSerialize

	def perform_create(self , serializer):
		serializer.save()


class EstudianteAPIList(generics.ListAPIView):
	serializer_class = EstudianteSerialize

	def get_queryset(self , *args , **kwargs):
		return Estudiante.objects.all()

# CREATE

def create(request):
	form = EstudianteForm(request.POST or None)
	if request.user.is_authenticated:
		message = "User Is Logged"
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect("list")		
	else:
		message = "User Must Be Logged"

	context = {
		"form": form,
		"message": message
	}
	return render(request, "estudiante/create.html", context)

# RETRIEVE

def list(request):
	queryset = Estudiante.objects.all()
	context = {
		"objetos": queryset
	}
	return render(request, "estudiante/list.html", context)


def detail(request, id):
 	queryset = Estudiante.objects.get(id=id)	
 	context = {
 		"object": queryset
 	}
 	return render(request, "estudiante/detail.html", context)


# UPDATE

def update(request, id):
	objeto = Estudiante.objects.get(id=id)
	if  request.method == "GET":
		form = EstudianteForm(instance=objeto )
	else:
		form = EstudianteForm(request.POST, instance=objeto )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form": form
	}
	return render(request, "estudiante/update.html", context)

# DELETE
def delete(request, id):
	objeto = Estudiante.objects.get(id=id)
	if request.method == "POST":
		objeto.delete()
		return redirect("list")
	context = {
		"object": objeto
	}
	return render(request, "estudiante/delete.html", context)

#################### Generic Views 

#List
class List(generic.ListView):
	template_name = "estudiante/list2.html"
	queryset = Estudiante.objects.filter()


	def get_queryset(self, *args, **kwargs):
		qs = Estudiante.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre__icontains=query))
		return qs

#Detail 

class Detail(generic.DetailView):
	template_name = "estudiante/detail2.html"
	model = Estudiante

#Create

class Create(generic.CreateView):
	template_name = "estudiante/create2.html"
	model = Estudiante
	fields = [
			"nombre",
			"apellido",
			"edad",
			"correo",
			"carrera",
			"semestre",
			"status"
		]
	success_url = reverse_lazy("list")

#Update 
class Update(generic.UpdateView):
	template_name = "estudiante/update2.html"
	model = Estudiante
	fields = [
			"nombre",
			"apellido",
			"edad",
			"correo",
			"carrera",
			"semestre",
			"status"
		]
	success_url = reverse_lazy("list")


#Delete

class Delete(generic.DeleteView):
	template_name = "estudiante/delete2.html"
	model = Estudiante
	success_url = reverse_lazy("list")

