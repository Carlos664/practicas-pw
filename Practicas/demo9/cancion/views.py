from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q


# Create your views here.

from .models import Cancion
from .forms import CancionForm
##Modulos necesarios para APIS 
from .serializers import CancionSerialize
from rest_framework import generics

#Vistas de las APIS
class CancionAPICreate(generics.CreateAPIView):
	serializer_class = CancionSerialize

	def perform_create(self , serializer):
		serializer.save()


class CancionAPIList(generics.ListAPIView):
	serializer_class = CancionSerialize

	def get_queryset(self , *args , **kwargs):
		return Cancion.objects.all()


# CREATE

def create(request):
	form = CancionForm(request.POST or None)
	if request.user.is_authenticated:
		message = "User Is Logged"
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect("list")
	else:
		message = "User Must Be Logged"

	context = {
		"form": form,
		"message": message
	}
	return render(request, "cancion/create.html", context)

# RETRIEVE
def list(request):
	queryset = Cancion.objects.all()
	context = {
		"cancions": queryset
	}
	return render(request, "cancion/list.html", context)

def detail(request, id):
 	queryset = Cancion.objects.get(id=id)	
 	context = {
 		"object": queryset
 	}
 	return render(request, "cancion/detail.html", context)


# UPDATE
def update(request, id):
	cancion = Cancion.objects.get(id=id)
	if  request.method == "GET":
		form = CancionForm(instance=cancion )
	else:
		form = CancionForm(request.POST, instance=cancion )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form": form
	}
	return render(request, "cancion/update.html", context)

# DELETE
def delete(request, id):
	cancion = Cancion.objects.get(id=id)
	if request.method == "POST":
		cancion.delete()
		return redirect("list")
	context = {
		"object": cancion
	}
	return render(request, "cancion/delete.html", context)

#Vistas genericas

class List(generic.ListView):
	template_name = "cancion/list2.html"
	queryset = Cancion.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Cancion.objects.all()
		query = self.request.GET.get('q' , None)
		if(query):
			qs = qs.filter( Q( titulo__icontains = query ) )
		return qs

class Detail(generic.DetailView):
	template_name = "cancion/detail2.html"
	model = Cancion

class Create(generic.CreateView):
	template_name = "cancion/create2.html"
	model = Cancion
	fields = [
			"titulo",
			"comositor",
			"album",
			"duracion",
			"genero",
		]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "cancion/update2.html"
	model = Cancion
	fields = [
			"titulo",
			"comositor",
			"album",
			"duracion",
			"genero",
		]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "cancion/delete2.html"
	model = Cancion
	success_url = reverse_lazy("list")