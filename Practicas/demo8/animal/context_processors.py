from .models import Animal
import time 

def messageVersion(request):
	context = {"msgVersion":"AnimalessApp v0.0.1",}
	return context

def countAnimal(request):
	context = {"countAnim": Animal.objects.count(),}
	return context

def getDate(self):
	context = {"date": time.strftime("%c")}
	return context