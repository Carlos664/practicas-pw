from rest_framework import serializers

from .models import Auto 


class AutoSerialize(serializers.ModelSerializer):
	class Meta:
		model = Auto
		fields = [
			"matricula",
			"marca",
			"año",
			"color",
			"tipo",
			"status"
		]
