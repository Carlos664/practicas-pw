from django import forms


from .models import Computadora

class ComputadoraForm(forms.ModelForm):
	class Meta:
		model = Computadora
		fields = [
			"modelo",
			"marca",
			"procesador",
			"ram",
			"disco",
			"estado",
		]