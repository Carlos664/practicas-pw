from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q


# Create your views here.

from .models import Animal
from .forms import AnimalForm

##Modulos necesarios para APIS 
from .serializers import AnimalSerialize
from rest_framework import generics

#Vistas de las APIS
class AnimalAPICreate(generics.CreateAPIView):
	serializer_class = AnimalSerialize

	def perform_create(self , serializer):
		serializer.save()


class AnimalAPIList(generics.ListAPIView):
	serializer_class = AnimalSerialize

	def get_queryset(self , *args , **kwargs):
		return Animal.objects.all()

# CREATE
def create(request):
	form = AnimalForm(request.POST or None)
	if request.user.is_authenticated:
		message = "User Is Logged"
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect("list")
	else:
		message = "User Must Be Logged"

	context = {
		"form": form,
		"message": message
	}
	return render(request, "animal/create.html", context)

# RETRIEVE
def list(request):
	queryset = Animal.objects.all()
	context = {
		"animals": queryset
	}
	return render(request, "animal/list.html", context)

def detail(request, id):
 	queryset = Animal.objects.get(id=id)	
 	context = {
 		"object": queryset
 	}
 	return render(request, "animal/detail.html", context)


# UPDATE
def update(request, id):
	animal = Animal.objects.get(id=id)
	if  request.method == "GET":
		form = AnimalForm(instance=animal )
	else:
		form = AnimalForm(request.POST, instance=animal )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form": form
	}
	return render(request, "animal/update.html", context)

# DELETE
def delete(request, id):
	animal = Animal.objects.get(id=id)
	if request.method == "POST":
		animal.delete()
		return redirect("list")
	context = {
		"object": animal
	}
	return render(request, "animal/delete.html", context)

#Vistas genericas

class List(generic.ListView):
	template_name = "animal/list2.html"
	queryset = Animal.objects.filter()


	def get_queryset(self, *args, **kwargs):
		qs = Animal.objects.all()
		query = self.request.GET.get('q' , None)
		if(query):
			qs = qs.filter( Q( nombre__icontains = query ) )

		return qs

class Detail(generic.DetailView):
	template_name = "animal/detail2.html"
	model = Animal


class Create(generic.CreateView):
	template_name = "animal/create2.html"
	model = Animal
	fields = [
			"nombre",
			"tipo",
			"ecosistema",
			"alimento",
			"extinto",
		]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "animal/update2.html"
	model = Animal
	fields = [
			"nombre",
			"tipo",
			"ecosistema",
			"alimento",
			"extinto",
		]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "animal/delete2.html"
	model = Animal
	success_url = reverse_lazy("list")
